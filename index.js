const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const ejs = require('ejs');
// const db = require('./config/db'); 
const mysql = require('mysql');



//Init app
const app = express(); 

app.use(bodyParser.urlencoded({ extended: true }));
//EJS
app.set('view engine', 'ejs');

//Public Folder
app.use(express.static(__dirname + '/public'));
// app.use(express.static(__dirname + '/public/uploads'));

// app.get('/', (req, res) => res.render('index'));

const port = process.env.PORT || 3000;

// MongoClient.connect(db.url, (err, database) => {
// 	if(err) return console.log(err);

// 	const dbBase = database.db('notable-1')
// 	require('./app/routes')(app, dbBase);
// 	app.listen(process.env.PORT || 3000)
// });

const db = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : 'ahextech',
	database : 'freshprint'
});

db.connect((err) => {
	if (err.fatal) {
    	console.trace('fatal error: ' + err.message);
 	 }
	console.log('MySql connected!');
	require('./app/routes')(app, db);
	app.listen(port,function(){
		console.log('Sever is listening at port', port)
	});
});





