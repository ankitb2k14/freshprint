# README #

This README would normally document whatever steps are necessary to get your application up and running.
# Project Title
FreshPrints

# About
This project is a simple image editor where user can do following -
1) Upload an image
2) Edit an image
3) Save or download an image
4) Retreive saved images


### How do I get set up? ###

1) open terminal
2) git clone https://ankitb2k14@bitbucket.org/ankitb2k14/freshprint.git
3)Go to project path
4) run cmd - npm install
5) run cmd -nodemon or npm start
6) go to localhost:30000 at your browser


### Who do I talk to? ###

Ankit
ankitb2k14@gmail.com