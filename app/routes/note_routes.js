var ObjectID = require('mongodb').ObjectID;
const multer = require('multer');
const path = require('path');

//Set Storage
const storage = multer.diskStorage({
	destination: './public/uploads/',
	filename: function(req, file, cb){
		cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
	}
});

//Init Upload
const upload = multer({
	storage: storage,
	limits: {
		fileSize: 1000000
	},
	fileFilter: function(req, file, cb){
		checkFileType(file, cb);
	}
}).single('file');

//check File Type
function checkFileType(file, cb){
	const filetypes = /jpeg|jpg|png|gif/;
	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
	const mimetype = filetypes.test(file.mimetype);

	if(mimetype && extname){
		return cb(null, true);
	}else{
		cb('Error: Images Only');
	}
}

module.exports = function(app, db){
	app.post('/upload', (req, res) => {
		upload(req, res, (err) => {
			console.log(req.file);
			if(err){
				res.status(400).send('An error has occured');
			}else{
				
				if(req.file == undefined){
					res.status(400).send('No file Send');
				} else{

					let file = { file: req.file.filename };
					let sql = 'INSERT INTO images SET ?';
					let query = db.query(sql, file, (err, result) => {
						console.log(db);
						if(err) 
							res.status(400).send('Please try after sometime');
						else
							res.status(200).send(result);
					});
				}
			}
		});
	});

	app.get('/image', (req, res) => {
		let sql = 'SELECT * FROM images';
		let query = db.query(sql , (err, result) => {	
			if(err) 
				res.status(400).send(`Please try after sometime ${err}`);
			else
				res.status(200).send(result);
		});
	});

	app.delete('/image/:id', (req, res) => {
		let sql = `DELETE FROM images WHERE id = ${req.params.id}`;
		let query = db.query(sql , (err, result) => {
			if(err) 
				res.status(400).send('Please try after sometime');
			else
				res.status(200).send(`Image ${req.params.id} has been deleted`);
		});
	});

	// app.delete('/image/:id', (req, res) => {
	// 	const id = req.params.id;
	// 	const details = {'_id': new ObjectID(id)};
	// 	db.collection('Image').remove(details, (err, item) => {
	// 		if(err){
	// 			res.send({ 'error': 'An error has occured' });
	// 		}else{
	// 			res.status(200).send('Note ' + id + ' deleted');
	// 		}
	// 	});
	// });

	// app.get('/:id', (req, res) => {
	// 	const id = req.params.id;
	// 	const details = {'_id': new ObjectID(id)};
	// 	db.collection('Image').findOne(details, (err, item) => {
	// 		if(err){
	// 			res.send({ 'error': 'An error has occured' });
	// 		}else{
	// 			res.render('index', {
	// 				msg: 'File uploaded',
	// 				file: 'uploads/' + item.file
	// 			})
	// 		}
	// 	});
	// });
};